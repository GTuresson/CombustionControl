function [dQ,CA50,IMEP] = HRA(p,params,tdc_offset)

if(size(p,1) > 1)
    p = p';
end

Tin = params.Tin; % Intake-manifold temperature at IVC
speed = params.speed; % engine speed in rpm
Tw = params.Tw; % cylinder-wall temperature
soi = params.soi; % start of fuel injection
Tw = params.Tw; % cylinder-wall temperature [K]
lambda = params.lambda; 
% Cylinder geometry
 % displacement volume [m^3]
l = params.l;  % connecting rod [m^3]
L = params.L;  % stroke [m^3]
B = params.B;  % bore [m^3]
IVC = params.IVC;
EVO = params.EVO;
rc = params.rc; % compression ratio

a = L/2; % crank radius
Vd = L*(B/2)^2*pi;
Vc = 1/(rc-1)*Vd;  % clearance volume 
R = l/a;    

res = 0.2; % pressure-signal resolution [CAD]
CAD = (IVC:res:EVO) + tdc_offset; % crank-angle degree from intake-valve closing to exhaust-valve opening with a 0.2 resolution

V = Vc + Vc/2*(rc-1)*(R + 1 - cos(pi*CAD/180) - (R^2 - sin(pi*CAD/180).^2).^(1/2)); % cylinder volume
 
gamma = 1.37; % assumed constant gamma
nR = p(1)*V(1)/Tin; % compute the nR constant in ideal gas law nRT = PV
T = p.*V/nR; % in-cylinder temperature by the assumption of the ideal gas law and constant mass. (approximation due to fuel injection).

p_mot = p(1)*(V(1)./V).^gamma; % assumed motored pressure curve, should be compared to motored engine data.

% derivative computation
dV = gradient(V,res);
dp = gradient(p,res);

% filtering of pressure derivative
[B_f,A_f] = butter(4,0.2); % filter coefficients
dp = filtfilt(B_f,A_f,dp); % zero-phase filtering

% Heat-Transfer Model, see Heywood p. 680.  dQ_ht = hc*A*(T-Tw),
% hc is a heat-transfer coefficient, Tw wall temp. and A cylinder area.

Ach = B^2/4*pi; % cylinder head area
Ap = Ach; % piston area
A = Ach + Ap + pi*B*L/2*(R + 1 - cos(pi*CAD/180) - (R^2 - sin(pi*CAD/180).^2).^(1/2)); % total area
Sp = 2*L*speed/60; % mean piston speed [m/s]

C1 = 2.28; %tuning parameter
C2 = 0.0032; %tuning parameter 

w =  C1*Sp + C2*(CAD > 0).*((Vd*Tin)/(V(1)*p(1))).*max(p-p_mot,0); % average cylinder velocity
hc = 3.26*B.^(-0.2).*(p/1000).^(0.8).*T.^(-0.55).*w.^0.8; % convective heat-transfer coefficient
conv = 60/(360*1200); % conversion from time to CAD
dQ_f = hc.*(T-Tw)/(10^6); % heat-transfer flux [MW/m^2] (data in Heywood are given in this unit);
dQ_ht = hc.*A.*(T-Tw)*conv; % heat-transfer rate
Q_ht = cumsum(dQ_ht)*res;

dQ_a = gamma./(gamma-1).*p.*dV + 1./(gamma-1).*V.*dp; % apparent heat-release rate (without heat-transfer)
dQ = gamma./(gamma-1).*p.*dV + 1./(gamma-1).*V.*dp + dQ_ht; % net heat-release rate (with heat-transfer)

dQ_a(1:200) = 0; % removed noise close to IVC
dQ(1:200) = 0; 

Q_a = cumsum(dQ_a)*res; 
Q = cumsum(dQ)*res; % accumulated heat-release

% compute CA50, CA10, CA90
[~,ind_CA50] = min(abs(Q - 0.5*max(Q)));
CA50 = CAD(ind_CA50);
[~,ind_CA90] = min(abs(Q - 0.9*max(Q)));
CA90 = CAD(ind_CA90);
[~,ind_CA10] = min(abs(Q - 0.1*max(Q)));
CA10 = CAD(ind_CA10);
CA90_10 = CA90-CA10;

% compute gamma from gas composition and temperature, gamma = cp(T)/cv(T).

R = 8.3144621;

% cp polynomials in T
cp_CO2 = (T <= 1000).*(0.24008*10 + 0.87351*10^(-2)*T - 0.66071*10^(-5)*T.^2 + 0.2002*10^(-8)*T.^3 + 0.63274*10^(-15)*T.^4)*R + (T > 1000).*(0.446*10 + 0.309*10^(-2)*T - 0.123*10^(-5)*T.^2 + 0.227*10^(-9)*T.^3  - 0.155*10^(-13)*T.^4)*R; 
cp_H2O = (T <= 1000).*(0.4071*10  - 0.11084*10^(-2)*T + 0.4152*10^(-5)*T.^2 - 0.29637*10^(-8)*T.^3 + 0.80702*10^(-12)*T.^4)*R + (T > 1000).*(0.271*10  + 0.294*10^(-2)*T - 0.802*10^(-6)*T.^2 + 0.102*10^(-9)*T.^3 - 0.48*10^(-14)*T.^4)*R;
cp_O2 =  (T <= 1000).*(0.36256*10  - 0.18782*10^(-2)*T + 0.7055*10^(-5)*T.^2 - 0.67635*10^(-8)*T.^3 + 0.21556*10^(-11)*T.^4)*R + (T > 1000).*(0.36256*10  + 0.73*10^(-3)*T - 0.19*10^(-6)*T.^2 + 0.36*10^(-10)*T.^3 - 0.289*10^(-14)*T.^4)*R;
cp_N2 =  (T <= 1000).*(0.36748*10  - 0.12082*10^(-2)*T + 0.2324*10^(-5)*T.^2 - 0.63218*10^(-9)*T.^3 - 0.22577*10^(-12)*T.^4)*R + (T > 1000).*(0.289*10  + 0.15082*10^(-2)*T - 0.572*10^(-6)*T.^2 + 0.99*10^(-10)*T.^3 - 0.65*10^(-14)*T.^4)*R;
y = 2.28; % H to C (2.2857 for n-heptane)
eps = 4/(4+y);
psi = 3.773;
cp_ub = 1/4.774*(3.773*cp_N2 + cp_O2); % cp unburnt gas
cp_b = (eps/lambda*cp_CO2 + 2*(1-eps)/lambda*cp_H2O + (1-1/lambda)*cp_O2 + psi*cp_N2)/((1-eps)/lambda + 1 + psi); % cp burnt gas

gamma_ub = cp_ub./(cp_ub-R); % cp unburnt gas
gamma_b = cp_b./(cp_b-R); % cp burnt gas
dist = normcdf(CAD,CA50,CA90_10/4); % interpolation between unburnt and burtn gas, based on computed combustion timing and duration
gamma_new = gamma_ub.*(1-dist) + gamma_b.*dist;

% recompute heat-release rate with updated gamma

dQ_gamma = gamma_new./(gamma_new-1).*p.*dV + 1./(gamma_new-1).*V.*dp + dQ_ht; % net heat-release rate (with heat-transfer)
dQ_gamma(1:200) = 0; % removed noise close to IVC

Q_gamma = cumsum(dQ_gamma)*res;

[~,ind_CA50] = min(abs(Q_gamma - 0.5*max(Q_gamma)));
CA50 = CAD(ind_CA50);

IMEP = res*p*dV'/Vd*10^-5;

end

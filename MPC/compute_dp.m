function dY = compute_dp(theta,Y)

p = Y(1);   
Tw = Y(2);
p_mot = Y(3);
NO = Y(4); % not currently used

% initialize parameters
CAD = -151:0.02:146;
[~,ind] = min(abs(CAD-theta));

p0 =  evalin('base','p0');
Tin = 302;
lambda = evalin('base','lambda');
EGR_ratio = evalin('base','EGR_ratio');
nEngine = 1200;
C1 = 2.2800;
C2 = 0.0032;

% cylinder geometry
Vd = 12.74*10^(-3)/6;
V_ivc = 0.002166582207529;
l = 255*10^-3;
L = 160*10^-3;
B = 130*10^-3;
a = L/2;

R = l/a;
Sp = 2*L*nEngine/60;

V = evalin('base','V_sim');
dV = evalin('base','dV_sim');
dQ = evalin('base','dQ_sim');
Q = evalin('base','Q_sim');

soi = -22.0;
V = V(ind);
dV = dV(ind);

Ach = B^2/4*pi;
Ap = Ach;
A = Ach + Ap + pi*B*L/2*(R + 1 - cos(pi*theta/180) - (R^2 - sin(pi*theta/180).^2).^(1/2));

nR = p0*(V_ivc)/Tin;
T = p.*V/nR;

% compute composition and gamma, NASA tabulated cp;

P_cpCO2 = [0.000000006888166  -0.000032997220525   0.057431249909167  23.073575308677736];
P_cpH2O = [-0.000000002651159   0.000007901047048   0.004389313147244  31.487597696305581];
P_cpO2 = [0.000000001483708  -0.000007708338981   0.015936685570169  25.060981350618935];
P_cpN2 = [0.000000000002768  -0.000000013857754   0.000022661949832  -0.009120029784096  30.114586316499540];

R = 8.3144621;

cp_CO2 = polyval(P_cpCO2,T); 
cp_H2O = polyval(P_cpH2O,T); 
cp_O2 = polyval(P_cpO2,T); 
cp_N2 = polyval(P_cpN2,T); 

y = 2.25;
eps = 4/(4+y);
psi = 3.773;
cp_ub = 1/4.774*(3.773*cp_N2 + cp_O2);
cp_b = (eps/lambda*cp_CO2 + 2*(1-eps)/lambda*cp_H2O + (1-1/lambda)*cp_O2 + psi*cp_N2)/((1-eps)/lambda + 1 + psi);

gamma_ub = cp_ub./(cp_ub-R);
gamma_b = cp_b./(cp_b-R);
dist = 1./(1+exp(-(ind-7810)/100));
gamma = (1-EGR_ratio)*(gamma_ub.*(1-dist) + gamma_b.*dist) + EGR_ratio*gamma_b;

% heat-transfer model, Woschi

help1 = A./(360*nEngine/60).*(T-Tw);
help2 = real(1.5*3.26*B.^(-0.2).*(p/1000).^(0.8).*T.^(-0.55));
help3 = (C1*Sp + C2*(theta > soi)*(Vd/V_ivc)*(max((p-p_mot),0)/p0)*Tin);

hc_w = help2.*help3.^(0.8);
dQ_ht = help1.*hc_w;

% pressure derivatives

dY(1,1) = -gamma*dV/V*p + (gamma-1)/V*(dQ(ind)-dQ_ht);
dY(3,1) = -gamma*dV/V*p_mot;

Tc = 273+60;
kc = 25;
Lc = 0.025;
mc = 25;
cp = 0.46*10^2;

% cylinder wall derivative

A_dyn = -2*(hc_w+kc/Lc)/mc/cp.*A./(360*nEngine/60);
B_dyn = [2*hc_w.*A./(360*nEngine/60)/mc/cp ; 2*kc*A./(360*nEngine/60)/mc/cp/Lc];
dY(2,1) = A_dyn*Tw + B_dyn(1)*T + B_dyn(2)*Tc;

dY(4,1) = 0; % did previously contain NO formation

end

% MPC for efficent NOx avoidance and LOAD tracking

clc
clear
close all
h_fig = figure('Color',[1 1 1]);
set(gca(h_fig),'Color',[1 1 1]);
set(gca(h_fig),'FontName','Times New Roman');
rng(1)

% noise
sigma_CA50 = 0.25;
sigma_Q = 25;

%%%%%%%%%%%%%% Run Simulation %%%%%%%%%

N_iters = 100; % simulation iterations

ignition_delay = 5;
u = [0,-5,2000,5,0,10]';
sigma = 2.0; % determines combustion duration
lambda = 2;
EGR_ratio = 0.0;
T_inlet = 300;
Tw0 = 465;
p0 = [];
options = odeset('RelTol',10^-4,'AbsTol',10^-8);

compute_volume

tracking = 0; % if pressure tracking, 0 here

%%%%%%%%%%%%%% Tuning Parameters %%%%%%%%%

Np = 5; % Control Horizon
alpha_0 = 0.00025;  % CA50 Tracking cost
alpha = alpha_0;
beta = 125; % IMEP Tracking cost
R_q = 125;  % m_f change cost
R_SOI = 0.45; % SOI change cost
rho = [100;0.5]; % constraint cost [pmax,NO]


% case 1:
% p_max = 110; % pmax limit
% NO_lim = 1000; % NO limit


% case 2:
p_max = 200; % pmax limit
NO_lim = 600; % NO limit



CA50_ref = 5; % CA50_ref limit
lb = [-600;-1.5]; % control action limit

%%%%%%%%%% Detection Parameters %%%%%%%%%%

threshold = 25; % dQ peak threshold
N_dQ = 3;% number of injections possible

%%%%%%%%%% Linearized Model %%%%%%%%%%%%

hc = 0.1;
k_ht = Area*hc;
kappa = 1.3;
help_1 = 1./(V.^kappa).*exp(-k_ht.*theta);
help_2 = (kappa-1).*V.^(kappa-1).*exp(k_ht.*theta);

%%%%%%%%%%%%%% Reference %%%%%%%%%%%%%%%%

n = 1; % number of active injections
Hessian = 1; %

%%%%%%%%%%%%%% MPC loop %%%%%%%%%%%%%%%%%%%%

qp_options = optimoptions('quadprog','Algorithm','active-set');

% vectors for storing data

US = [];
CA50s = [];
T_EVOs = [];
T_EXs = [];
IMEPS = [];
NOX = [];

IMEP_ref = 10;

% IMEP & CA50 refs
IMEP_REFS = [5*ones(N_iters/4,1);10*ones(N_iters/4,1);5*ones(N_iters/4,1);10*ones(N_iters/4+10,1)];
CA50_REFS = [[5*ones(N_iters/4,1);5*ones(N_iters/4,1);5*ones(N_iters/4,1);5*ones(N_iters/4+10,1)] [5*ones(N_iters/4,1);5*ones(N_iters/4,1);5*ones(N_iters/4,1);5*ones(N_iters/4+10,1)] [5*ones(N_iters/4,1);5*ones(N_iters/4,1);5*ones(N_iters/4,1);5*ones(N_iters/4+10,1)]] ;
ALPHA_P =   [1*ones(N_iters/3,1);0*ones(N_iters/3,1);0.5*ones(N_iters/3+10,1)];

tdc_ind = 756;

for i = 1:N_iters
    
    alpha_p = ALPHA_P(i+1);
    compute_p_ref
    
    %%%%%%%%%%% Run Simulation %%%%%%%%
    
    compute_dQ;
    compute_volume
    
    Qc = u(1) + u(3) + u(5);
    
    dQ_real = [Q1(1:10:end); Q2(1:10:end); Q3(1:10:end)];
    
    dQ_sim = Q1 + Q2 + Q3;
    Q_sim = cumsum(dQ_sim)*0.02;
    
    [~,Y] = ode23(@compute_dp,theta_sim,Y0,options);
    p = Y(1:10:end,1)*10^-5;
    T = p.*V'/nR*10^5;
    IMEP = dV*p*0.2/Vd;
    p_max_val = max(p);
    
    %%%%%%%%%% dQ Detection %%%%%%%%%%%
    
    [dQ,CA50] = detect_dQ(N_dQ,dQ_sim(1:10:end),threshold,u,tracking);
    CA50 = [theta(CA50(1));theta(CA50(2));theta(CA50(3))];
    CA50s(i,:) = CA50;
    n_active = logical(sum(dQ,2)*0.2 > 100);
    n_active(2) = 1;
    n_INJ = sum(n_active);
    n_active_all = logical([n_active(1) n_active(1) n_active(2) n_active(2) n_active(3) n_active(3)])';
    
    IMEP_ref = IMEP_REFS(i);
    CA50_ref = CA50_REFS(i,:);
    IMEPS(i) = IMEP;
    
    %%%%%%%%%%%%%% Compute Qp matrices %%%%%%%%%%%%%%%%%
    
    [f,H,dp,ddQ] = compute_qp(dQ(n_active,:),alpha,beta,IMEP,IMEP_ref,CA50(n_active),CA50_ref(n_active),u(n_active_all),dV,Vd,help_1,help_2,n_INJ,Hessian,tracking,p_ref,p);
    f_grad = f;
    dT = dp(:,end)*V(end)/nR*10^5;
    dT_full = dp.*repmat(V,n_INJ*2,1)/nR*10^5;
    min_dist = 6;
    
    %%%%%%%%%%%%%%%%%%% Downsampling pmax constraint %%%%
    
    down_sample = 5;
    pre_CAD = 5;
    aft_CAD = 30;
    
    p_pmax = p(tdc_ind-5*pre_CAD:down_sample:tdc_ind+aft_CAD*5);
    dp_pmax = dp(:,tdc_ind-5*pre_CAD:down_sample:tdc_ind+aft_CAD*5);
    
    %%%%%%%%%%%%%%%%%%%% NO model & Linearization %%%%%%%
    
    NO = compute_NO(T',p'*10^5,V,lambda,EGR_ratio,Q_sim(1:10:end),local_lambda,EGR_comp,C_rad);
    NOX(i) = NO(end);
    dNOx = [];
    
    switch n_INJ
        
        case 1
            
            dQ_NO(1,:) = Q_sim(1:10:end) + cumsum(ddQ(1,:))*0.2;
            dQ_NO(2,:) = cumsum([zeros(1,5)  dQ(2,1:end-5)])*0.2;
            
        case 2
            
            dQ_NO(1,:) = Q_sim(1:10:end) + cumsum(ddQ(1,:))*0.2;
            dQ_NO(3,:) = Q_sim(1:10:end) + cumsum(ddQ(3,:))*0.2;
            
            dQ_NO(2,:) = cumsum(dQ(2,:))*0.2 + cumsum([zeros(1,5)  dQ(1,1:end-5)])*0.2;
            dQ_NO(4,:) = cumsum(dQ(1,:))*0.2 + cumsum([zeros(1,5)  dQ(2,1:end-5)])*0.2;
            
        case 3
            
            dQ_NO(1,:) = Q_sim(1:10:end) + cumsum(ddQ(1,:))*0.2;
            dQ_NO(3,:) = Q_sim(1:10:end) + cumsum(ddQ(3,:))*0.2;
            dQ_NO(5,:) = Q_sim(1:10:end) + cumsum(ddQ(5,:))*0.2;
            
            dQ_NO(2,:) = cumsum(dQ(3,:) + dQ(2,:))*0.2 + cumsum([zeros(1,5)  dQ(1,1:end-5)])*0.2;
            dQ_NO(4,:) = cumsum(dQ(3,:) + dQ(1,:))*0.2 + cumsum([zeros(1,5)  dQ(2,1:end-5)])*0.2;
            dQ_NO(6,:) = cumsum(dQ(1,:) + dQ(2,:))*0.2 + cumsum([zeros(1,5)  dQ(3,1:end-5)])*0.2;
            
    end
    
    
    for i_NOx = 1:(2*n_INJ);
        
        dNO(i_NOx,:) = compute_NO(T' + dT_full(i_NOx,:),(p' + dp(i_NOx,:))*10^5,V,lambda,EGR_ratio,dQ_NO(i_NOx,:),local_lambda,EGR_comp,C_rad);
        dNOx(i_NOx) = dNO(i_NOx,end)-NO(end);
        
        if( mod(i_NOx,2) == 0 )
            dNOx(i_NOx) = min(-25,dNOx(i_NOx)); % to avoid strange differentiation errors
        end
        
    end
    
    %%% Build Qp matrices over horizon %%%
    
    [H,f,A_ineq,d_ineq,LB] = build_matrices(H,f,Np,R_q,R_SOI,dp_pmax',p_pmax,p_max,lb,min_dist,u(n_active_all),NOX(i),dNOx,NO_lim,tracking,n_active,rho);
    X0 = zeros(Np*n_INJ*2,1);
    
    %%%%%%%%%%%%%% Compute du %%%%%%%%%%%%%%%%%
    
    [U,FVAL,EXITFLAG,OUTPUT,LAMBDA] = quadprog(H,f,A_ineq,d_ineq,[],[],LB,-LB,[],qp_options);
    US(:,i) = u;
    
    %%% update control signal with saturiation %%
    
    du = U(1:n_INJ*2);
    u(n_active_all) = u(n_active_all) + du;
    
    for kk = [2 4 6];
        
        u(kk) = max(-10,min(35,u(kk)));
        
    end
    
    u(6) = max(u(6),u(4) + min_dist);
    u(2) = min(u(2),u(4) - min_dist);
    
    for kk = [1 5];
        
        if(u(kk) < 200)
            u(kk) = 0 ;
        end
        
    end
   
    %%%%%%% Add Injections %%%%%
    
    if((NOX(i) > NO_lim - 200) && any([u(1) u(3)] < 100))
        
        u(1) = 500;
        u(2) = u(4) - 5;
        u(3) = u(3) - 500;
        
    end
    
    if((p_max_val > p_max - 5) && any([u(1) u(3)] < 100))
        
        u(1) = 500;
        u(2) = u(4) - 5;
        u(3) = u(3) - 500;
        
    end
    
    %%%%%%% Gain Scheduling %%%%%
    
    if((NOX(i) > NO_lim - 200) || (p_max_val > p_max - 5))
    
        alpha = alpha_0/3;
        
    else
        
        alpha = alpha_0;
        
    end
    
    
    %%%%%% Plot %%%%%%%%%%%%%%%%%%
    
    U = U(1:end-2);
    plot_progress;
        
end
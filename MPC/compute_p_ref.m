%  Computes a Seiliger cycle
kappa_ref = 1.375;
Cv = 946;
Cp = kappa_ref*Cv;

p_mot = 10^-5*p0*(V(1)./V).^kappa_ref;
p2 = p_mot(tdc_ind);
T2 = T_inlet*(V./V(tdc_ind)).^(kappa_ref-1);
deltaT = alpha_p*M_fuel/(M_fuel+M_air)*Q_LHV/Cv;
T3 = T2 + deltaT;
p3 = p2*(T3/T2);
deltaT = (1-alpha_p)*M_fuel/(M_fuel+M_air)*Q_LHV/Cp;
T4 = T3 + deltaT;
V4 = V(tdc_ind)*T4/T3;
[~,exp_ind] = min(abs(V(tdc_ind:end)-V4));
p_exp = p3*(V4./V).^kappa;
p_ref = [p_mot(1:tdc_ind) p3*ones(exp_ind,1)' p_exp(exp_ind+tdc_ind+1:end)]';

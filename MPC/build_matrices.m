function [H,f,A_ineq,d,LB] = build_matrices(H,f,Np,R_q,R_SOI,dp,p,p_max,lb,min_dist,u,NO,dNO,NO_lim,tracking,n_active,rho)
% this function assemples H and f over the prediction horizon and
% computes the A <= b matrices of the Qp

n = sum(n_active); 
Q_margin = 500;
A_ineq = [];
A_ineq_pmax = [];
A_ineq_NO = [];

HH = [];
RR = [];
F = [];

d_pmax = [];
d_prr = [];
d_T_ex = [];
d_NO = [];

LB = [];

switch n % number of injections
    
    case 3
        
        R = diag([1/(R_q^2) 1/(R_SOI^2) 1/(R_q^2) 1/(R_SOI^2) 1/(R_q^2) 1/(R_SOI^2)]);
        A = eye(6*Np) + diag(-ones(6*(Np-1),1),6)';
        A_inv = inv(A);
        
        if(tracking)
            A_ineq_1 = [-1 0 0 0 0 0;    % u_fuel > 0
                0 0 -1 0 0 0;    % u_fuel > 0
                0 0  0 0 -1 0;   % u_fuel > 0
                0 1  0 -1 0 0;   % fulfill injection distance
                0 0  0  1 0 -1]; % fulfill injection distance
            
            d = [u(1);u(3);u(5); -min_dist - u(2) + u(4);-min_dist-u(4) + u(6)];
        
        else
            
            A_ineq_1 = [-1 0 0 0 0 0;    % u_fuel > 0
                0 0 -1 0 0 0;    % u_fuel > 0
                0 0  0 0 -1 0;   % u_fuel > 0
                1 0 -1 0 0 0;    % middle injection is largest
                0 0 -1 0 1 0;    % middle injection is largest
                0 1  0 -1 0 0;   % fulfill injection distance
                0 0  0  1 0 -1]; % fulfill injection distance
            
            d = [u(1);u(3);u(5);u(3)-u(1)-Q_margin;u(3)-u(5)-Q_margin; -min_dist - u(2) + u(4);-min_dist-u(4) + u(6)];
            
        end
        
        d = repmat(d,Np,1);
        lb = [lb;lb;lb];
        
        for i = 1:Np
            if(i == Np)
                HH = blkdiag(HH,H);
                RR = blkdiag(RR,R);
                F = [F;f];
                A_ineq = blkdiag(A_ineq,A_ineq_1);
                
                A_ineq_pmax = blkdiag(A_ineq_pmax,dp);
                d_pmax = [d_pmax;p_max-p];
                
                A_ineq_NO = blkdiag(A_ineq_NO,dNO);
                d_NO = [d_NO;NO_lim-NO];
                LB = [LB;lb];
                
            else
                
                HH = blkdiag(HH,H);
                RR = blkdiag(RR,R);
                F = [F;f];
                A_ineq = blkdiag(A_ineq,A_ineq_1);
                A_ineq_pmax = blkdiag(A_ineq_pmax,dp);
                d_pmax = [d_pmax;p_max-p];
                
                A_ineq_NO = blkdiag(A_ineq_NO,dNO);
                d_NO = [d_NO;NO_lim-NO];
                LB = [LB;lb];
                
            end
        end
        
        if(~tracking)
            
            n_orig = size(A_ineq,1);
            n_pmax = size(A_ineq_pmax,1);

            n_NO = size(A_ineq_NO,1);
            
            A_ineq = [A_ineq;A_ineq_pmax;A_ineq_NO];
            d = [d;d_pmax;d_NO];

            
        end
        
        H = A_inv'*HH*A_inv + RR;
        f = A_inv'*F;
        
        A_ineq = A_ineq*A_inv;
        
        if(~tracking)
            f = [f;0;0];
            H = blkdiag(H,rho(1),rho(2));
            
            A_ineq = [A_ineq [zeros(n_orig,1);-ones(n_pmax,1);zeros(n_NO,1)] [zeros(n_orig+n_pmax,1);-ones(n_NO,1)]];            
            A_ineq = [A_ineq ; zeros(1,size(A_ineq,2)-1) -1];
            A_ineq = [A_ineq ; zeros(1,size(A_ineq,2)-2) -1 0];
            
            d = [d;0;0];
            LB = [LB;-10^6;-10^6];
        end
        
        
    case 2
        
        R = diag([1/(R_q^2) 1/(R_SOI^2) 1/(R_q^2) 1/(R_SOI^2)]);
        A = eye(4*Np) + diag(-ones(4*(Np-1),1),4)';
        A_inv = inv(A);
        
        if(n_active(1))
            
            A_ineq_1 = [-1 0 0 0 ;
                0 0 -1 0 ;
                1 0 -1 0 ;
                0 1 0 -1 ];
            
            d = [u(1);u(3);u(3)-u(1)-Q_margin; -min_dist - u(2) + u(4)];
            
        else
            
            A_ineq_1 = [-1 0 0 0 ;
                0 0 -1 0 ;
                -1 0 1 0 ;
                0 1 0 -1 ];
            
            d = [u(1);u(3);u(1)-u(3)+Q_margin; -min_dist - u(2) + u(4)];
            
        end
        
        d = repmat(d,Np,1);
        lb = [lb;lb];
        
        for i = 1:Np
            if(i == Np)
                
                HH = blkdiag(HH,H);
                RR = blkdiag(RR,R);
                F = [F;f];
                A_ineq = blkdiag(A_ineq,A_ineq_1);
                
                A_ineq_pmax = blkdiag(A_ineq_pmax,dp);
                d_pmax = [d_pmax;p_max-p];
                A_ineq_NO = blkdiag(A_ineq_NO,dNO);
                d_NO = [d_NO;NO_lim-NO];
                LB = [LB;lb];
                
            else
                
                HH = blkdiag(HH,H);
                RR = blkdiag(RR,R);
                F = [F;f];
                A_ineq = blkdiag(A_ineq,A_ineq_1);
                A_ineq_pmax = blkdiag(A_ineq_pmax,dp);
                d_pmax = [d_pmax;p_max-p];
                
                A_ineq_NO = blkdiag(A_ineq_NO,dNO);
                d_NO = [d_NO;NO_lim-NO];
                
                LB = [LB;lb];
                
            end
        end
        
        if(~tracking)
            % Add slack variables
            n_orig = size(A_ineq,1);
            n_pmax = size(A_ineq_pmax,1);
            n_NO = size(A_ineq_NO,1);
            A_ineq = [A_ineq;A_ineq_pmax;A_ineq_NO];
            d = [d;d_pmax;d_NO];
        end
        
        H = A_inv'*HH*A_inv + RR;
        f = A_inv'*F;
        A_ineq = A_ineq*A_inv;
        
        if(~tracking)
            f = [f;0;0];
            H = blkdiag(H,rho(1),rho(2));

            A_ineq = [A_ineq [zeros(n_orig,1);-ones(n_pmax,1);zeros(n_NO,1)] [zeros(n_orig+n_pmax,1);-ones(n_NO,1)]];

            A_ineq = [A_ineq ; zeros(1,size(A_ineq,2)-1) -1];
            A_ineq = [A_ineq ; zeros(1,size(A_ineq,2)-2) -1 0];
            
            d = [d;0;0];
            LB = [LB;-10^6;-10^6];
        end
        
    case 1
        
        
        R = diag([1/(R_q^2) 1/(R_SOI^2)]);
        A = eye(2*Np)+diag(-ones(2*(Np-1),1),2)';
        A_inv = inv(A);
        
        A_ineq_1 = [-1 0];
        
        d = u(1);
        d = repmat(d,Np,1);
        lb = [lb];
        
        for i = 1:Np
            
            if(i == Np)
                HH = blkdiag(HH,H);
                RR = blkdiag(RR,R);
                F = [F;f];
                A_ineq = blkdiag(A_ineq,A_ineq_1);
                
                A_ineq_pmax = blkdiag(A_ineq_pmax,dp);
                d_pmax = [d_pmax;p_max-p];
                
                A_ineq_NO = blkdiag(A_ineq_NO,dNO);
                d_NO = [d_NO;NO_lim-NO];
                
                LB = [LB;lb];
                
            else
                
                HH = blkdiag(HH,H);
                RR = blkdiag(RR,R);
                F = [F;f];
                A_ineq = blkdiag(A_ineq,A_ineq_1);
                A_ineq_pmax = blkdiag(A_ineq_pmax,dp);
                d_pmax = [d_pmax;p_max-p];
                
                A_ineq_NO = blkdiag(A_ineq_NO,dNO);
                d_NO = [d_NO;NO_lim-NO];
                
                LB = [LB;lb];
                
            end
        end
        
        if(~tracking)
            % Add slack variables
            n_orig = size(A_ineq,1);
            n_pmax = size(A_ineq_pmax,1);
            n_NO = size(A_ineq_NO,1);
            
            A_ineq = [A_ineq;A_ineq_pmax;A_ineq_NO];
            d = [d;d_pmax;d_NO];
            
        end
        
        H = A_inv'*HH*A_inv + RR;
        f = A_inv'*F;
        A_ineq = A_ineq*A_inv;
        
        if(~tracking)
            f = [f;0;0];
            H = blkdiag(H,rho(1),rho(2));
            
            A_ineq = [A_ineq [zeros(n_orig,1);-ones(n_pmax,1);zeros(n_NO,1)] [zeros(n_orig+n_pmax,1);-ones(n_NO,1)]];

            A_ineq = [A_ineq ; zeros(1,size(A_ineq,2)-1) -1];
            A_ineq = [A_ineq ; zeros(1,size(A_ineq,2)-2) -1 0];

            d = [d;0;0];
            LB = [LB;-10^6;-10^6];
            
        end
        
end
end
% computes 3 Vibe functions


% Vibe 1
Dtheta_b = 2*sigma(1);
Dtheta_d = sigma(1);
theta_ign = u(2) + ignition_delay - Dtheta_d - Dtheta_b/2 + randn*sigma_CA50;

m_Vibe = log(log(1 - 0.1)/log(1 - 0.9))/(log(Dtheta_d) - log(Dtheta_d + Dtheta_b)) - 1;
a_Vibe = -log(1 - 0.1)*((Dtheta_d + Dtheta_b)/Dtheta_d)^(m_Vibe + 1);
Q_Vibe_1 = (theta_sim > theta_ign).*(1 - exp( -a_Vibe*(max((theta_sim - theta_ign),0.001)./(Dtheta_b + Dtheta_d)).^(m_Vibe+1) ));

if(u(1) > 100)
Q1 = gradient(Q_Vibe_1,0.02)*(u(1) + sigma_Q*randn);
else
Q1 = gradient(Q_Vibe_1,0.02)*(u(1));
end

% Vibe 2
Dtheta_b = 2*sigma(1);
Dtheta_d = sigma(1);
theta_ign = u(4) + ignition_delay - Dtheta_d - Dtheta_b/2 + randn*sigma_CA50;

m_Vibe = log(log(1 - 0.1)/log(1 - 0.9))/(log(Dtheta_d) - log(Dtheta_d + Dtheta_b)) - 1;
a_Vibe = -log(1 - 0.1)*((Dtheta_d + Dtheta_b)/Dtheta_d)^(m_Vibe + 1);
Q_Vibe_2 = (theta_sim > theta_ign).*(1 - exp( -a_Vibe*(max((theta_sim - theta_ign),0.001)./(Dtheta_b + Dtheta_d)).^(m_Vibe+1) ));

if(u(3) > 100)
Q2 = gradient(Q_Vibe_2,0.02)*(u(3) + sigma_Q*randn);
else
Q2 = gradient(Q_Vibe_2,0.02)*(u(3));
end


% Vibe 3
Dtheta_b = 2*sigma(1);
Dtheta_d = sigma(1);
theta_ign = u(6) + ignition_delay - Dtheta_d - Dtheta_b/2 + randn*sigma_CA50;

m_Vibe = log(log(1 - 0.1)/log(1 - 0.9))/(log(Dtheta_d) - log(Dtheta_d + Dtheta_b)) - 1;
a_Vibe = -log(1 - 0.1)*((Dtheta_d + Dtheta_b)/Dtheta_d)^(m_Vibe + 1);
Q_Vibe_3 = (theta_sim > theta_ign).*(1 - exp( -a_Vibe*(max((theta_sim - theta_ign),0.001)./(Dtheta_b + Dtheta_d)).^(m_Vibe+1) ));

if(u(5) > 100)
Q3 = gradient(Q_Vibe_3,0.02)*(u(5) + sigma_Q*randn);
else
Q3 = gradient(Q_Vibe_3,0.02)*(u(5));
end
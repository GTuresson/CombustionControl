%%%%%%%%%%%%%% plot progress %%%%%%%%%%%%%%%%%

h_fig = figure(1);clf

set(gcf,'color','white')
figPos = get(0,'defaultfigureposition');
width = 800;
height = 700;
set(gcf,'Position', [800, 100, width, height]);
cmap = [0.21569,0.49412,0.72157;0.89412,0.10196,0.10980;122/255,16/255,228/255;0.3020    0.6863    0.2902];
fontsize = 12;

p_pred = [];
u_pred = 0;
IMEP_pred = [];
p_max_pred = [];

for ii = 1:Np
    
    u_pred = u_pred + U(1+2*n_INJ*(ii-1):ii*2*n_INJ);
    p_pred(ii,:) = p + dp'*u_pred;
    IMEP_pred(ii) = dV*p_pred(ii,:)'*0.2/Vd;
    dp_pred(ii,:) = gradient(p_pred(ii,:),0.2);
    
end

subplot(221);hold on;box on;grid on
plot(theta,p,'linewidth',2,'color',cmap(1,:))
plot(theta,p_pred(2,:),'-','linewidth',2,'color',cmap(1,:))
plot(theta,p_pred(3,:),'--','linewidth',2,'color',cmap(1,:))
plot(theta,NO/20,'-','linewidth',2,'color',cmap(3,:))
plot(theta,ones(1,size(theta,2))*NO_lim/20,'--','linewidth',2,'color',cmap(3,:))
plot(theta,ones(1,size(theta,2))*p_max,'--','linewidth',2,'color',cmap(1,:))
plot(theta,p_pred(Np,:),':','color',cmap(1,:))
xlim([-10,35])
ylim([-10 150])

plot(theta,(dQ_real(1,:) + dQ_real(2,:) + dQ_real(3,:))/10,'k','linewidth',2)
plot(theta,dQ(1,:)/10,'--','linewidth',2,'color',cmap(1,:))
plot([CA50(1) CA50(1)],[-10 150],'linewidth',2,'color',cmap(1,:))
plot(theta,dQ(2,:)/10,'--','linewidth',2,'color',cmap(2,:))
plot([CA50(2) CA50(2)],[-10 150],'linewidth',2,'color',cmap(2,:))
plot(theta,dQ(3,:)/10,'--','linewidth',2,'color',cmap(4,:))
plot([CA50(3) CA50(3)],[-10 150],'linewidth',2,'color',cmap(4,:))

xlabel('\theta [CAD]', 'FontSize', fontsize)
ylabel('p [bar]', 'FontSize', fontsize)
title(['# Injections = ' num2str(n_INJ)], 'FontSize', fontsize)
box on;grid on

subplot(222)
stairs(IMEPS,'-','linewidth',2,'color',cmap(1,:));hold on
stairs(IMEP_REFS,'k--','linewidth',2)
stairs([i-1:i+Np-1]',[IMEPS(i) IMEP_pred],'--','color',cmap(1,:))
ylabel('p_{IMEP} [bar]', 'FontSize', fontsize, 'FontSize', fontsize)
xlabel('cycle [-]', 'FontSize', fontsize, 'FontSize', fontsize)
ylim([0 15])
xlim([1 N_iters])
box on;grid on

subplot(2,2,[3,4]);
stairs(NOX,'linewidth',2,'color',cmap(3,:));hold on
stairs(NO_lim*ones(N_iters,1),'k--','linewidth',2)
ylabel('NO [ppm]', 'FontSize', fontsize)
xlabel('cycle [-]', 'FontSize', fontsize)
ylim([0 1000])


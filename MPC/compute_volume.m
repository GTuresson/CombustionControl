%%%%%%%%%%%%%%%%%%%%%%% Compute Volume %%%%%%%%%%%%%%%%%%%%%%%%%%%

Vd = 12.74*10^(-3)/6;
l = 255*10^-3;
L = 160*10^-3;
B = 130*10^-3;
a = L/2;
rc = 17;
Vc = 1/(rc-1)*Vd;   
R = l/a;
theta = (-151:0.2:146);
Ach = B^2/4*pi;
Ap = Ach;
Area = Ach + Ap + pi*B*L/2*(R + 1 - cos(pi*theta/180) - (R^2 - sin(pi*theta/180).^2).^(1/2));
V_ivc = 0.002166582207529;
V = Vc + Vc/2*(rc-1)*(R + 1 - cos(pi*theta/180) - (R^2 - sin(pi*theta/180).^2).^(1/2));
dV = gradient(V,0.2);
theta_sim = (-151:0.02:146);
V_sim = Vc + Vc/2*(rc-1)*(R + 1 - cos(pi*theta_sim/180) - (R^2 - sin(pi*theta_sim/180).^2).^(1/2));
dV_sim = gradient(V_sim,0.02);

%%%%%%%%%%%%%%%%%%%%%%% Computes Initial Conditions %%%%%%%%%%%%%%%%%%%%%%%%%%%


Qc = u(1) + u(3) + u(5);
Q_LHV = 43.4*10^6;
M_fuel = Qc/(Q_LHV);
M_air = 14.5*lambda.*M_fuel;
M_egr = EGR_ratio./(1-EGR_ratio).*M_air;
p0 = 1.4884e+05;

nR = p0*V(1)/T_inlet;
Y0 = [p0,Tw0,p0,0]; %

local_lambda = 2;
C_rad = 0;
EGR_comp = 1;

assignin('base','V_sim', V_sim)
assignin('base','dV_sim', dV_sim)

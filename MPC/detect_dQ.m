function [dQ,CA50] = detect_dQ(Np,dQ,threshold,u,tracking)

%%%%%%%%%%%%%%%%% Detection %%%%%%%%%%%%%%%%%%%%%%

[B_qf,A_qf] = butter(4,0.1); % filter coefficients

if(~tracking)
    
    switch Np
        
        case 3
            
            X = dQ;
            Ph = threshold;
            Pd = 15;
            
            pks = [];
            locs = [];
            
            Indx = find(X > Ph); % find peaks larger than threshold
            
            trend = sign(diff(X));
            idx = find(trend==0); % Find flats
            N = length(trend);
            
            for i=length(idx):-1:1,
                % Back-propagate trend for flats
                if trend(min(idx(i)+1,N))>=0,
                    trend(idx(i)) = 1;
                else
                    trend(idx(i)) = -1; % Flat peak
                end
            end
            
            idx  = find(diff(trend)==-2)+1;  % Get all the peaks
            locs = intersect(Indx,idx);      % Keep peaks above MinPeakHeight
            pks  = X(locs);
            
            if isempty(pks) || Pd==1,
                dQ = [dQ;dQ;dQ];
                return
            end
            
            % Order peaks from large to small
            [pks, idx] = sort(pks,'descend');
            locs = locs(idx);
            
            idelete = ones(size(locs))<0;
            
            for i = 1:length(locs),
                if ~idelete(i),
                    % If the peak is not in the neighborhood of a larger peak, find
                    % secondary peaks to eliminate.
                    idelete = idelete | (locs>=locs(i)-Pd)&(locs<=locs(i)+Pd);
                    idelete(i) = 0; % Keep current peak
                end
            end
            
            pks(idelete) = []; %delete close peaks
            locs(idelete) = [];
            
            
            if length(pks)>Np,
                locs = locs(1:Np);
                pks  = pks(1:Np);
            end
            
            [locs, idx] = sort(locs,'ascend');
            pks = pks(idx);
            
            CA50_ind = locs;
            
            if(size(CA50_ind,2) > 2)
                
                CA50_m_1 = CA50_ind(1);
                CA50_m_2 = CA50_ind(2);
                CA50_m_3 = CA50_ind(3);
                three_Q_peaks_detected = true;
                
            else if(size(CA50_ind,2) > 1)
                    
                    if(u(1) > 100)
                        CA50_m_1 = CA50_ind(1);
                        CA50_m_2 = CA50_ind(2);
                        CA50_m_3 = 0;
                    else if(u(3) > 100)
                            CA50_m_1 = 0;
                            CA50_m_2 = CA50_ind(1);
                            CA50_m_3 = CA50_ind(2);
                        end
                    end
                    
                    two_Q_peaks_detected = true;
                    three_Q_peaks_detected = false;
                    
                else
                    CA50_m_1 = 0;
                    CA50_m_2 = CA50_ind(1);
                    CA50_m_3 = 0;
                    
                    two_Q_peaks_detected = false;
                    three_Q_peaks_detected = false;
                    
                end
            end
            
            if(three_Q_peaks_detected)
                
                intersection_1 = dQ(min(CA50_m_1,CA50_m_2):max(CA50_m_1,CA50_m_2));
                [~,min_ind_1] = min(intersection_1);
                
                intersection_2 = dQ(min(CA50_m_2,CA50_m_3):max(CA50_m_2,CA50_m_3));
                [~,min_ind_2] = min(intersection_2);
                
                dQ_1_est = dQ;
                dQ_2_est = dQ;
                dQ_3_est = dQ;
                
                dQ_1_est(CA50_m_1+min_ind_1:end) = 0;
                dQ_2_est(1:CA50_m_1+min_ind_1) = 0;
                dQ_2_est(CA50_m_2+min_ind_2:end) = 0;
                dQ_3_est(1:CA50_m_2+min_ind_2) = 0;
                
            else if(two_Q_peaks_detected)
                    
                    if(u(1) > 100)
                        
                        intersection = dQ(min(CA50_m_1,CA50_m_2):max(CA50_m_1,CA50_m_2));
                        [~,min_ind] = min(intersection);
                        dQ_1_est = dQ;
                        dQ_2_est = dQ;
                        dQ_3_est = dQ;
                        dQ_1_est(CA50_m_1 + 1 + min_ind:end) = 0;
                        dQ_2_est(1:CA50_m_1 + min_ind) = 0;
                        dQ_3_est(:) = 0;
                        
                    else if(u(3) > 100)
                            
                            intersection = dQ(min(CA50_m_2,CA50_m_3):max(CA50_m_2,CA50_m_3));
                            [~,min_ind] = min(intersection);
                            dQ_1_est = dQ;
                            dQ_2_est = dQ;
                            dQ_3_est = dQ;
                            dQ_2_est(CA50_m_2 + 1 + min_ind:end) = 0;
                            dQ_3_est(1:CA50_m_2 + min_ind) = 0;
                            dQ_1_est(:) = 0;
                            
                        end
                    end
                    
                else
                    
                    dQ_1_est = dQ;
                    dQ_2_est = dQ;
                    dQ_3_est = dQ;
                    dQ_1_est(:) = 0;
                    dQ_3_est(:) = 0;
                    
                    
                end
            end
    end
    
    dQ_1_est = max(filtfilt(B_qf,A_qf,dQ_1_est),0);
    dQ_2_est = max(filtfilt(B_qf,A_qf,dQ_2_est),0);
    dQ_3_est = max(filtfilt(B_qf,A_qf,dQ_3_est),0);
    
    dQ = [dQ_1_est;dQ_2_est;dQ_3_est];
    CA50 = max([CA50_m_1,CA50_m_2,CA50_m_3],1);
    
    
else  % if tracking
    
    switch Np
        
        case 3
            
            X = dQ;
            Ph = threshold;
            Pd = 25;
            
            pks = [];
            locs = [];
            
            Indx = find(X > Ph);
            
            trend = sign(diff(X));
            idx = find(trend==0); % Find flats
            N = length(trend);
            
            for i=length(idx):-1:1,
                % Back-propagate trend for flats
                if trend(min(idx(i)+1,N))>=0,
                    trend(idx(i)) = 1;
                else
                    trend(idx(i)) = -1; % Flat peak
                end
            end
            
            idx  = find(diff(trend)==-2)+1;  % Get all the peaks
            locs = intersect(Indx,idx);      % Keep peaks above MinPeakHeight
            pks  = X(locs);
            
            if isempty(pks) || Pd==1,
                dQ = [dQ;dQ;dQ];
                return
            end
            
            % Order peaks from large to small
            [pks, idx] = sort(pks,'descend');
            locs = locs(idx);
            
            idelete = ones(size(locs))<0;
            
            for i = 1:length(locs),
                if ~idelete(i),
                    % If the peak is not in the neighborhood of a larger peak, find
                    % secondary peaks to eliminate.
                    idelete = idelete | (locs>=locs(i)-Pd)&(locs<=locs(i)+Pd);
                    idelete(i) = 0; % Keep current peak
                end
            end
            
            pks(idelete) = [];
            locs(idelete) = [];
            
            
            if length(pks)>Np,
                locs = locs(1:Np);
                pks  = pks(1:Np);
            end
            
            [locs, idx] = sort(locs,'ascend');
            pks = pks(idx);
            
            CA50_ind = locs;
            
            % peak allocation depending on how many peaks detected
            
            if(size(CA50_ind,2) > 2)
                
                CA50_m_1 = CA50_ind(1);
                CA50_m_2 = CA50_ind(2);
                CA50_m_3 = CA50_ind(3);
                three_Q_peaks_detected = true;
                
            else if(size(CA50_ind,2) > 1)
                    
                    if((u(1) > 100) && (u(3) > 100)) % only allocate peaks if fuel is injected
                        CA50_m_1 = CA50_ind(1);
                        CA50_m_2 = CA50_ind(2);
                        CA50_m_3 = 0;
                    else if(u(3) > 100 && (u(2) > 100))
                            CA50_m_1 = 0;
                            CA50_m_2 = CA50_ind(1);
                            CA50_m_3 = CA50_ind(2);
                        else
                            CA50_m_1 = CA50_ind(1);
                            CA50_m_2 = 0;
                            CA50_m_3 = CA50_ind(2);
                        end
                    end
                    
                    
                    two_Q_peaks_detected = true;
                    three_Q_peaks_detected = false;
                    
                else
                    
                    [~,ind] = max([u(1) u(3) u(5)]);
                    
                    if(ind == 1)
                        CA50_m_1 = CA50_ind(1);
                        CA50_m_2 = 0;
                        CA50_m_3 = 0;
                    else if(ind == 2)
                            CA50_m_1 = 0;
                            CA50_m_2 = CA50_ind(1);
                            CA50_m_3 = 0;
                        else if(ind == 3)
                                CA50_m_1 = 0;
                                CA50_m_2 = 0;
                                CA50_m_3 = CA50_ind(1);
                                
                            end
                        end
                    end
                    
                    two_Q_peaks_detected = false;
                    three_Q_peaks_detected = false;
                    
                end
            end
            
            if(three_Q_peaks_detected)
                % compute dQ
                
                intersection_1 = dQ(min(CA50_m_1,CA50_m_2):max(CA50_m_1,CA50_m_2));
                [~,min_ind_1] = min(intersection_1);
                
                intersection_2 = dQ(min(CA50_m_2,CA50_m_3):max(CA50_m_2,CA50_m_3));
                [~,min_ind_2] = min(intersection_2);
                
                dQ_1_est = dQ;
                dQ_2_est = dQ;
                dQ_3_est = dQ;
                % compute dQ
                
                dQ_1_est(CA50_m_1+min_ind_1:end) = 0;
                dQ_2_est(1:CA50_m_1+min_ind_1) = 0;
                dQ_2_est(CA50_m_2+min_ind_2:end) = 0;
                dQ_3_est(1:CA50_m_2+min_ind_2) = 0;
                
            else if(two_Q_peaks_detected)
                    % compute dQ
                    
                    if((u(1) > 100) && (u(3) > 100))
                        
                        intersection = dQ(min(CA50_m_1,CA50_m_2):max(CA50_m_1,CA50_m_2));
                        [~,min_ind] = min(intersection);
                        dQ_1_est = dQ;
                        dQ_2_est = dQ;
                        dQ_3_est = dQ;
                        dQ_1_est(CA50_m_1 + 1 + min_ind:end) = 0;
                        dQ_2_est(1:CA50_m_1 + min_ind) = 0;
                        dQ_3_est(:) = 0;
                        
                    else if((u(3) > 100) && (u(5) > 100))
                            % compute dQ
                            
                            intersection = dQ(min(CA50_m_2,CA50_m_3):max(CA50_m_2,CA50_m_3));
                            [~,min_ind] = min(intersection);
                            dQ_1_est = dQ;
                            dQ_2_est = dQ;
                            dQ_3_est = dQ;
                            dQ_2_est(CA50_m_2 + 1 + min_ind:end) = 0;
                            dQ_3_est(1:CA50_m_2 + min_ind) = 0;
                            dQ_1_est(:) = 0;
                            
                            
                        else if((u(1) > 100) && (u(5) > 100))
                                % compute dQ
                                
                                intersection = dQ(min(CA50_m_1,CA50_m_3):max(CA50_m_1,CA50_m_3));
                                [~,min_ind] = min(intersection);
                                dQ_1_est = dQ;
                                dQ_2_est = dQ;
                                dQ_3_est = dQ;
                                dQ_1_est(CA50_m_1 + 1 + min_ind:end) = 0;
                                dQ_2_est(:) = 0;
                                dQ_3_est(1:CA50_m_1 + min_ind) = 0;
                                
                            end
                        end
                    end
                    
                else
                    
                    % compute dQ
                    dQ_1_est = dQ;
                    dQ_2_est = dQ;
                    dQ_3_est = dQ;
                    
                    
                    if(u(1) > 100)
                        dQ_2_est(:) = 0;
                        dQ_3_est(:) = 0;
                    else if(u(3) > 100)
                            dQ_1_est(:) = 0;
                            dQ_3_est(:) = 0;
                        else if(u(5) > 100)
                                dQ_1_est(:) = 0;
                                dQ_2_est(:) = 0;
                            end
                        end
                    end
                end
            end
    end
    
    dQ_1_est = max(filtfilt(B_qf,A_qf,dQ_1_est),0);
    dQ_2_est = max(filtfilt(B_qf,A_qf,dQ_2_est),0);
    dQ_3_est = max(filtfilt(B_qf,A_qf,dQ_3_est),0);
    
    dQ = [dQ_1_est;dQ_2_est;dQ_3_est];
    CA50 = max([CA50_m_1,CA50_m_2,CA50_m_3],1);
    
end
end
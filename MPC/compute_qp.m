function [f,H,dp,ddQ] = compute_qp(dQ,alpha,beta,IMEP,IMEP_ref,CA50,CA50_ref,x,dV,V_d,help_1,help_2,n,Hessian,tracking,p_ref,p)

% computes H f and differentiates p and Q w.r.t inptus.

IMEP_err = IMEP_ref - IMEP;
IMEP_SI_conv = 10^5*V_d;

switch n % for different number of injections
    
    case 1 
        
        dQ_1_est = dQ;

        sum_1 = max(sum(dQ_1_est*0.2),1);

        Q_tot = sum(dQ_1_est)*0.2;

        dQ_x1 = dQ_1_est'/sum_1; % dQ pratial derivative w.r.t fuel energy
        dQ_x2 = -2.5*([0; diff(dQ_1_est')] + [diff(dQ_1_est'); 0]); % dQ pratial derivative w.r.t timing

        dL_x1 = sum(help_1.*cumsum(help_2.*dQ_x1')*0.2.*dV)*0.2*10^-5/V_d; % IMEP pratial derivative w.r.t fuel energy
        dL_x2 = sum(help_1.*cumsum(help_2.*dQ_x2')*0.2.*dV)*0.2*10^-5/V_d; % IMEP pratial derivative w.r.t timing

        f_IMEP = [dL_x1;dL_x2];
        f_Q = [sum(dQ_x1); sum(dQ_x2)]*0.2;
        
        f(1,1) = 1/2*(CA50_ref(1)-CA50(1))^2;
        f(2,1) = -x(1)*(CA50_ref(1)-CA50(1));
        
        f = -beta*IMEP_err*f_IMEP + alpha*f ;
        
        dp_x1 = help_1.*cumsum(help_2.*dQ_x1')*0.2*10^-5;
        dp_x2 = help_1.*cumsum(help_2.*dQ_x2')*0.2*10^-5;
        
        if(tracking) % tracking problem gradient
            f = -alpha*[dp_x1; dp_x2]*(p_ref-p);
        end
        
        dp = [dp_x1; dp_x2];
        ddQ = [dQ_x1'; dQ_x2'];
        
        if(Hessian) % compute Hessian
            
            dQ_x1x2 = dQ_x2/sum_1; 
            dQ_x2x1 = dQ_x1x2;
            dQ_x2x2 = 2.5*([0; diff(dQ_x2)] + [diff(dQ_x2); 0]);

            dL_x1x2 = sum(help_1.*cumsum(help_2.*dQ_x1x2')*0.2.*dV)*0.2*10^-5/V_d;
            dL_x2x1 = dL_x1x2;
            dL_x2x2 = sum(help_1.*cumsum(help_2.*dQ_x2x2')*0.2.*dV)*0.2*10^-5/V_d;

            H_IMEP = [0       dL_x1x2; 
                      dL_x2x1 dL_x2x2];
        
            H_Q = [0            sum(dQ_x1x2); 
                   sum(dQ_x2x1) sum(dQ_x2x2)]*0.2;
            
            H(1,1) = 0;
            H(1,2) = (CA50_ref - CA50(1))*(-1);
            H(2,1) = (CA50_ref - CA50(1))*(-1);
            H(2,2) = 1;
            
            H = beta*(f_IMEP*f_IMEP') - beta*(IMEP_err)*H_IMEP + alpha*H;
            
            if(tracking)
                
            H = alpha*[dp_x1; dp_x2]*[dp_x1; dp_x2]';
                    
            end
            
        else
    
        H = zeros(2,2);
    
        end   
        
    case 2
            
        dQ_1_est = dQ(1,:);
        dQ_2_est = dQ(2,:);
        
        sum_1 = max(sum(dQ_1_est*0.2),1);
        sum_2 = max(sum(dQ_2_est*0.2),1);

        Q_tot = sum(dQ_1_est + dQ_2_est)*0.2;

        dQ_x1 = dQ_1_est'/sum_1;
        dQ_x2 = -2.5*([0; diff(dQ_1_est')] + [diff(dQ_1_est'); 0]);

        dL_x1 = sum(help_1.*cumsum(help_2.*dQ_x1')*0.2.*dV)*0.2*10^-5/V_d;
        dL_x2 = sum(help_1.*cumsum(help_2.*dQ_x2')*0.2.*dV)*0.2*10^-5/V_d;

        dQ_x3 = dQ_2_est'/sum_2;
        dQ_x4 = -2.5*([0; diff(dQ_2_est')] + [diff(dQ_2_est'); 0]);

        dL_x3 = sum(help_1.*cumsum(help_2.*dQ_x3')*0.2.*dV)*0.2*10^-5/V_d;
        dL_x4 = sum(help_1.*cumsum(help_2.*dQ_x4')*0.2.*dV)*0.2*10^-5/V_d;

        f_IMEP = [dL_x1; dL_x2; dL_x3; dL_x4];

        f_Q = [sum(dQ_x1); 0; sum(dQ_x3); 0];
        
        f(1,1) = 1/2*(CA50_ref(1)-CA50(1))^2;
        f(2,1) = -x(1)*(CA50_ref(1)-CA50(1));
        f(3,1) = 1/2*(CA50_ref(2)-CA50(2))^2;
        f(4,1) = -x(3)*(CA50_ref(2)-CA50(2));
        
%         f_IMEP(2) = 0;
%         f_IMEP(4) = 0;
        
        f   = -beta*IMEP_err*f_IMEP + alpha*f;
        
        dp_x1 = help_1.*cumsum(help_2.*dQ_x1')*0.2*10^-5;
        dp_x2 = help_1.*cumsum(help_2.*dQ_x2')*0.2*10^-5;
        dp_x3 = help_1.*cumsum(help_2.*dQ_x3')*0.2*10^-5;
        dp_x4 = help_1.*cumsum(help_2.*dQ_x4')*0.2*10^-5;
        
        if(tracking)
            f = -alpha*[dp_x1; dp_x2; dp_x3; dp_x4]*(p_ref-p);
        end
        


        dp = [dp_x1; dp_x2; dp_x3; dp_x4];
        ddQ = [dQ_x1'; dQ_x2'; dQ_x3'; dQ_x4'];
        
        if(Hessian)
            
            dQ_x1x2 = dQ_x2/sum_1;
            dQ_x2x1 = dQ_x1x2;
            dQ_x2x2 = 2.5*([0; diff(dQ_x2)] + [diff(dQ_x2); 0]);

            dL_x1x2 = sum(help_1.*cumsum(help_2.*dQ_x1x2')*0.2.*dV)*0.2*10^-5/V_d;
            dL_x2x1 = dL_x1x2;
            dL_x2x2 = sum(help_1.*cumsum(help_2.*dQ_x2x2')*0.2.*dV)*0.2*10^-5/V_d;

            dQ_x3x4 = dQ_x4/sum_2;
            dQ_x4x3 = dQ_x3x4;
            dQ_x4x4 = 2.5*([0; diff(dQ_x4)] + [diff(dQ_x4); 0]);

            dL_x3x4 = sum(help_1.*cumsum(help_2.*dQ_x3x4')*0.2.*dV)*0.2*10^-5/V_d;
            dL_x4x3 = dL_x3x4;
            dL_x4x4 = sum(help_1.*cumsum(help_2.*dQ_x4x4')*0.2.*dV)*0.2*10^-5/V_d;

            H_IMEP = [0 1*dL_x1x2 0 0; 
                      1*dL_x2x1 dL_x2x2 0 0;
                      0 0 0 1*dL_x3x4;
                      0 0 1*dL_x4x3 dL_x4x4];
      
            H_Q = [0 sum(dQ_x1x2) 0 0; 
                   sum(dQ_x2x1) sum(dQ_x2x2) 0 0;
                   0 0 0 sum(dQ_x3x4);
                   0 0 sum(dQ_x4x3) sum(dQ_x4x4)]*0.2;
   
    
            H(1,1) = 0;
            H(1,2) = (CA50_ref(1) - CA50(1))*(-1);
            H(2,1) = (CA50_ref(1) - CA50(1))*(-1);
            H(2,2) = 1;   
             
            
            H(3,3) = 0;
            H(3,4) = (CA50_ref(2) - CA50(2))*(-1);
            H(4,3) = (CA50_ref(2) - CA50(2))*(-1);
            H(4,4) = 1;
            
            H =    1*(beta*(f_IMEP*f_IMEP') - beta*(IMEP_err)*H_IMEP + alpha*H);
            
            if(tracking)
                    H = alpha*[dp_x1; dp_x2; dp_x3; dp_x4]*[dp_x1; dp_x2; dp_x3; dp_x4]';
            end
                
        else
    
            H = zeros(4,4);
    
        end

    case 3
        
            dQ_1_est = dQ(1,:);
            dQ_2_est = dQ(2,:);
            dQ_3_est = dQ(3,:);
 
            sum_1 = max(sum(dQ_1_est*0.2),1);
            sum_2 = max(sum(dQ_2_est*0.2),1);
            sum_3 = max(sum(dQ_3_est*0.2),1);

            Q_tot = sum(dQ_1_est + dQ_2_est + dQ_3_est)*0.2;

            dQ_x1 = dQ_1_est'/sum_1;
            dQ_x2 = -2.5*([0; diff(dQ_1_est')] + [diff(dQ_1_est'); 0]);

            dL_x1 = sum(help_1.*cumsum(help_2.*dQ_x1')*0.2.*dV)*0.2*10^-5/V_d;
            dL_x2 = sum(help_1.*cumsum(help_2.*dQ_x2')*0.2.*dV)*0.2*10^-5/V_d;

            dQ_x3 = dQ_2_est'/sum_2;
            dQ_x4 = -2.5*([0; diff(dQ_2_est')] + [diff(dQ_2_est'); 0]);

            dL_x3 = sum(help_1.*cumsum(help_2.*dQ_x3')*0.2.*dV)*0.2*10^-5/V_d;
            dL_x4 = sum(help_1.*cumsum(help_2.*dQ_x4')*0.2.*dV)*0.2*10^-5/V_d;

            dQ_x5 = dQ_3_est'/sum_3;
            dQ_x6 = -2.5*([0; diff(dQ_3_est')] + [diff(dQ_3_est'); 0]);

            dL_x5 = sum(help_1.*cumsum(help_2.*dQ_x5')*0.2.*dV)*0.2*10^-5/V_d;  
            dL_x6 = sum(help_1.*cumsum(help_2.*dQ_x6')*0.2.*dV)*0.2*10^-5/V_d;

            f_IMEP = [dL_x1;dL_x2;dL_x3;dL_x4;dL_x5;dL_x6];

            f_Q = [sum(dQ_x1);0;sum(dQ_x3);0;sum(dQ_x5);0]*0.2;
            
            
            f(1,1) = 1/2*(CA50_ref(1)-CA50(1))^2;
            f(2,1) = -x(1)*(CA50_ref(1)-CA50(1));
            f(3,1) = 1/2*(CA50_ref(2)-CA50(2))^2;
            f(4,1) = -x(3)*(CA50_ref(2)-CA50(2));
            f(5,1) = 1/2*(CA50_ref(3)-CA50(3))^2;
            f(6,1) = -x(5)*(CA50_ref(3)-CA50(3));
            
            f = -beta*IMEP_err*f_IMEP + alpha*f;

            
            dp_x1 = help_1.*cumsum(help_2.*dQ_x1')*0.2*10^-5;
            dp_x2 = help_1.*cumsum(help_2.*dQ_x2')*0.2*10^-5;
            dp_x3 = help_1.*cumsum(help_2.*dQ_x3')*0.2*10^-5;
            dp_x4 = help_1.*cumsum(help_2.*dQ_x4')*0.2*10^-5;
            dp_x5 = help_1.*cumsum(help_2.*dQ_x5')*0.2*10^-5;
            dp_x6 = help_1.*cumsum(help_2.*dQ_x6')*0.2*10^-5;
            
            if(tracking)
                
            f = -alpha*[dp_x1; dp_x2; dp_x3; dp_x4; dp_x5; dp_x6]*(p_ref-p);
            
            end
            
            dp = [dp_x1;dp_x2;dp_x3;dp_x4;dp_x5;dp_x6];
            ddQ = [dQ_x1';dQ_x2';dQ_x3';dQ_x4';dQ_x5';dQ_x6'];
            
            if(Hessian)
                
                dQ_x1x2 = dQ_x2/sum_1;
                dQ_x2x1 = dQ_x1x2;
                dQ_x2x2 = 2.5*([0; diff(dQ_x2)] + [diff(dQ_x2); 0]);
                
                dL_x1x2 = sum(help_1.*cumsum(help_2.*dQ_x1x2')*0.2.*dV)*0.2*10^-5/V_d;
                dL_x2x1 = dL_x1x2;
                dL_x2x2 = sum(help_1.*cumsum(help_2.*dQ_x2x2')*0.2.*dV)*0.2*10^-5/V_d;

                dQ_x3x4 = dQ_x4/sum_2;
                dQ_x4x3 = dQ_x3x4;
                dQ_x4x4 = 2.5*([0; diff(dQ_x4)] + [diff(dQ_x4); 0]);

                dL_x3x4 = sum(help_1.*cumsum(help_2.*dQ_x3x4')*0.2.*dV)*0.2*10^-5/V_d;
                dL_x4x3 = dL_x3x4;
                dL_x4x4 = sum(help_1.*cumsum(help_2.*dQ_x4x4')*0.2.*dV)*0.2*10^-5/V_d;

                dQ_x5x6 = dQ_x6/sum_3;
                dQ_x6x5 = dQ_x5x6;
                dQ_x6x6 = 2.5*([0; diff(dQ_x6)] + [diff(dQ_x6); 0]);

                dL_x5x6 = sum(help_1.*cumsum(help_2.*dQ_x5x6')*0.2.*dV)*0.2*10^-5/V_d;
                dL_x6x5 = dL_x5x6;
                dL_x6x6 = sum(help_1.*cumsum(help_2.*dQ_x6x6')*0.2.*dV)*0.2*10^-5/V_d;

                H_IMEP = [0 dL_x1x2 0 0 0 0; 
                          dL_x2x1 dL_x2x2 0 0 0 0;
                          0 0 0 dL_x3x4 0 0;
                          0 0 dL_x4x3 dL_x4x4 0 0;
                          0 0 0 0 0 dL_x5x6; 
                          0 0 0 0 dL_x6x5 dL_x6x6];
      
                H_Q = [0 sum(dQ_x1x2) 0 0 0 0; 
                       sum(dQ_x2x1) sum(dQ_x2x2) 0 0 0 0;
                       0 0 0 sum(dQ_x3x4) 0 0;
                       0 0 sum(dQ_x4x3) sum(dQ_x4x4) 0 0;
                       0 0 0 0 0 sum(dQ_x5x6); 
                       0 0 0 0 sum(dQ_x6x5) sum(dQ_x6x6)]*0.2;
   
                H(1,1) = 0;
                H(1,2) = (CA50_ref(1) - CA50(1))*(-1);
                H(2,1) = (CA50_ref(1) - CA50(1))*(-1);
                H(2,2) = 1;   
             
            
                H(3,3) = 0;
                H(3,4) = (CA50_ref(2) - CA50(2))*(-1);
                H(4,3) = (CA50_ref(2) - CA50(2))*(-1);
                H(4,4) = 1;
            
            
                H(5,5) = 0;
                H(5,6) = (CA50_ref(3) - CA50(3))*(-1);
                H(6,5) = (CA50_ref(3) - CA50(3))*(-1);
                H(6,6) = 1;
   
                H =    beta*(f_IMEP*f_IMEP') - beta*(IMEP_err)*H_IMEP + alpha*H;
                 
                if(tracking)
                    H = alpha*[dp_x1; dp_x2; dp_x3; dp_x4; dp_x5; dp_x6]*[dp_x1; dp_x2; dp_x3; dp_x4; dp_x5; dp_x6]';
                end
            else
    
                H = zeros(6,6);
    
            end

end


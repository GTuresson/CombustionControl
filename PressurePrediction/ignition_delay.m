function tau = ignition_delay(params)

soi = params.soi;

l = params.l;
L = params.L;
rc = params.rc;
B = params.B;
Vd = pi*L*(B/2)^2;
Vc = Vd/(rc-1);
a = L/2;
R = l/a;
V0 = params.V_ivc;
conv = params.speed/60*360*10^-3;

V = Vc + Vc/2*(rc-1)*(R + 1 - cos(pi*soi/180) - (R^2 - sin(pi*soi/180).^2).^(1/2));

gamma = 1.38;

% Heywood, Table 10.3

A = 0.0405;
n = 0.757;
EaR = 5475;

% cylinder-state at injection

T = (V0/V)^(gamma-1)*params.Tin;
p = (V0/V)^gamma*params.p0*10^-5;
    
tau = A*p^(-n)*exp(EaR/T)*conv; % Ignition delay in CAD

end
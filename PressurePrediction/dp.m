function dP = dp(theta,P,params)

% comutes pressure derivatives w.r.t. motored and fired cylinder pressure

% extract parameters
Tw = params.Tw;

V_ivc = params.V_ivc;
IVC = params.IVC;
EVO = params.EVO;
l = params.l;
L = params.L;
B = params.B;
rc = params.rc;
Vd = pi*L*(B/2)^2;
Vc = Vd/(rc-1);
a = L/2;
R = l/a;
speed = params.speed;
Sp = 2*L*speed/60;

p0 = params.p0;
Tin = params.Tin;
EGR = params.EGR;
lambda = params.lambda;

C1 = params.C1;
C2 = params.C2;

soi = params.soi;

CAD = params.CAD;
[~,ind] = min(abs(CAD-theta));

% compute V and dV

V = Vc + Vc/2*(rc-1)*(R + 1 - cos(pi*theta/180) - (R^2 - sin(pi*theta/180).^2).^(1/2));
dV =  pi/180*Vd/2*(sin(pi*theta/180) + sin(pi*theta/180)*cos(pi*theta/180)/(sqrt(R^2 - sin(pi*theta/180).^2)));

% load Q
Q = params.Q(ind);
dQ = params.dQ(ind);

Ach = B^2/4*pi;
Ap = Ach;
A = Ach + Ap + pi*B*L/2*(R + 1 - cos(pi*theta/180) - (R^2 - sin(pi*theta/180).^2).^(1/2));

p = P(1);   
nR = p0*(V_ivc)/Tin;
T = p.*V/nR;
% assignin('base', 'Tevo_tmp', T);

P_cpCO2 = [0.000000006888166  -0.000032997220525   0.057431249909167  23.073575308677736];
P_cpH2O = [-0.000000002651159   0.000007901047048   0.004389313147244  31.487597696305581];
P_cpO2 = [0.000000001483708  -0.000007708338981   0.015936685570169  25.060981350618935];
P_cpN2 = [0.000000000002768  -0.000000013857754   0.000022661949832  -0.009120029784096  30.114586316499540];

R = 8.3144621;

cp_CO2 = polyval(P_cpCO2,T); 
cp_H2O = polyval(P_cpH2O,T); 
cp_O2 = polyval(P_cpO2,T); 
cp_N2 = polyval(P_cpN2,T); 
y = 2.25;
eps = 4/(4+y);
psi = 3.773;
cp_ub = 1/4.774*(3.773*cp_N2 + cp_O2);
cp_b = (eps/lambda*cp_CO2 + 2*(1-eps)/lambda*cp_H2O + (1-1/lambda)*cp_O2 + psi*cp_N2)/((1-eps)/lambda + 1 + psi);

gamma_ub = cp_ub./(cp_ub-R);
gamma_b = cp_b./(cp_b-R);
Q_tot = params.Q(end);
gamma = (1-EGR)*(gamma_ub.*(1-Q/Q_tot) + gamma_b.*Q/Q_tot) + EGR*gamma_b;


p_mot = P(2);
soi = params.soi;

% heat-release model k_i are help variables

k1 = A./(360*speed/60).*(T-Tw);
k2 = real(3.26*B.^(-0.2).*(p/1000).^(0.8).*T.^(-0.55));
k3 = (C1*Sp + C2*(theta > soi)*(Vd/V_ivc)*(max((p-p_mot),0)/p0)*Tin);

hc_w = k2.*k3.^(0.8);
dQ_ht = k1.*hc_w;

dP(1,1) = -gamma*dV/V*p + (gamma-1)/V*(dQ - dQ_ht);
dP(2,1) = -gamma*dV/V*p;

end

% static maps for intake conditions

% interpolates intake conditions w.r.t. injected fuel amount

p0max = 2.0*10^5;
p0min = 1.0*10^5;

T0max = 333;
T0min = 303;

mf_max = 200;

t_mf = max(min(params.mf,mf_max),0)/mf_max;

params.p0 = t_mf*p0max + (1-t_mf)*p0min;
params.Tin = t_mf*T0min + (1-t_mf)*T0max;

M_fuel = params.mf*10^-6;
M_tot = params.p0*params.V_ivc/(287.058*params.Tin);
M_air = (1-params.EGR)*M_tot;
params.lambda = M_air/14.5/M_fuel;


%% uncomment to plot relation

% mf = 0:10:200;
% figure(1);clf;hold on
% 
% p0max = 2.0*10^5;
% p0min = 1.0*10^5;
% 
% T0max = 333;
% T0min = 303;
% 
% mf_max = 200;
% 
% for i=1:size(mf,2)
% 
% params.mf = mf(i);
% 
% t_mf = max(min(params.mf,mf_max),0)/mf_max;
% 
% params.p0 = t_mf*p0max + (1-t_mf)*p0min;
% params.Tin = t_mf*T0min + (1-t_mf)*T0max;
% 
% M_fuel = params.mf*10^-6;
% M_tot = params.p0*params.V_ivc/(287.058*params.Tin);
% M_air = (1-params.EGR)*M_tot;
% params.lambda = M_air/14.5/M_fuel;
% 
% subplot(311);hold on
% plot(mf(i),params.lambda,'o')
% ylabel('\lambda')
% xlabel('m_f [mg]')
% 
% ylim([0 5])
% 
% subplot(312);hold on
% plot(mf(i),params.Tin,'o')
% ylabel('T_{in} [K]')
% xlabel('m_f [mg]')
% 
% subplot(313);hold on
% plot(mf(i),params.p0,'o')
% ylabel('p_{0} [bar]')
% xlabel('m_f [mg]')
% 
% end
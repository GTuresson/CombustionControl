function [deltap,dIMEP] = deltap(dQ,p,params)
% Pressure-prediction method presented in
% Ingesson, Gabriel, et al. "A model-based injection-timing strategy for combustion-timing control." SAE International Journal of Engines 8.2015-01-0870 (2015): 1012-1020.


% Cylinder geometry
% displacement volume [m^3]
l = params.l;  % connecting rod [m^3]
L = params.L;  % stroke [m^3]
B = params.B;  % bore [m^3]
IVC = params.IVC;
EVO = params.EVO;
rc = params.rc; % compression ratio
res = params.res;
tdc_offset = params.tdc_offset;
Tin = params.Tin;
p0 = params.p0;
V_ivc = params.V_ivc;
C1 = params.C1;
C2 = params.C2;
soi = params.soi;
speed = params.speed;
Sp = 2*L*speed/60; % mean piston speed [m/s]

a = L/2; % crank radius
Vd = L*(B/2)^2*pi;
Vc = 1/(rc-1)*Vd;  % clearance volume 
R = l/a;    

CAD = (IVC:res:EVO) + tdc_offset; % crank-angle degree from intake-valve closing to exhaust-valve opening with a 0.2 resolution

V = Vc + Vc/2*(rc-1)*(R + 1 - cos(pi*CAD/180) - (R^2 - sin(pi*CAD/180).^2).^(1/2)); % cylinder volume
dV = gradient(V,res);
Ach = B^2/4*pi; % cylinder head area
Ap = Ach; % piston area
A = Ach + Ap + pi*B*L/2*(R + 1 - cos(pi*CAD/180) - (R^2 - sin(pi*CAD/180).^2).^(1/2)); % total area

nR = p(1)*V(1)/Tin; 
kappa = 1.38;
p_mot = p(1)*(V(1)./V).^kappa; % assumed motored pressure curve, should be compared to motored engine data.

% numerical differentiation of heat-transfer w.r.t to p

dp = 100; % change in p [pa]

T = (p + dp).*V/nR;
w =  C1*Sp + C2*(CAD > 0).*((Vd*Tin)/(V(1)*p(1))).*max(p + dp - p_mot,0); 
hc = 3.26*B.^(-0.2).*((p + dp)/1000).^(0.8).*T.^(-0.55).*w.^0.8*60/(360*1200); 

mu_plus = (kappa-1)*A.*Tin/p0/V_ivc.*hc.*(p+dp); % forward step

T = p.*V/nR;
w =  C1*Sp + C2*(CAD > 0).*((Vd*Tin)/(V(1)*p(1))).*max(p - p_mot,0); 
hc = 3.26*B.^(-0.2).*(p/1000).^(0.8).*T.^(-0.55).*w.^0.8*60/(360*1200); 

mu = (kappa-1)*A.*Tin/p0/V_ivc.*hc.*p;

dmu = (mu_plus - mu)/dp; % differential


% compute pressure dev w.r.t. 
help_1 = 1./(V.^kappa).*exp(-cumsum(dmu)*res);
help_2 = (kappa-1).*V.^(kappa-1).*exp(cumsum(dmu)*res);

dQ_u1 = dQ/sum(dQ*res)*(params.Q_LHV*10^-6); % heat-release derivative w.r.t m_f
dQ_u2 = -0.5*([0 diff(dQ)] + [diff(dQ) 0])/res; % heat-release derivative w.r.t soi

deltap(1,:) = help_1.*cumsum(help_2.*dQ_u1)*res; % pressure derivative w.r.t. m_f
deltap(2,:) = help_1.*cumsum(help_2.*dQ_u2)*res; % pressure derivative w.r.t. soi

dIMEP(1) = sum(deltap(1,:).*dV)*res*10^-5/Vd; % IMEP derivative w.r.t m_f
dIMEP(2) = sum(deltap(2,:).*dV)*res*10^-5/Vd; % IMEP derivative w.r.t soi

end
% %******************************************************************************************
% COPYRIGHT  � 2015
% THE REGENTS OF THE UNIVERSITY OF MICHIGAN
% ALL RIGHTS RESERVED
% 
% PERMISSION IS GRANTED TO USE, COPY AND REDISTRIBUTE THIS CODE AND ITS DATA PACKAGE FOR NONCOMMERCIAL EDUCATION AND RESEARCH PURPOSES,
% SO LONG AS NO FEE IS CHARGED, AND SO LONG AS THE COPYRIGHT NOTICE ABOVE, THIS GRANT OF PERMISSION, AND THE DISCLAIMER BELOW APPEAR 
% IN ALL COPIES MADE; AND SO LONG AS THE NAME OF THE UNIVERSITY OF MICHIGAN IS NOT USED IN ANY ADVERTISING OR PUBLICITY PERTAINING 
% TO THE USE OR DISTRIBUTION OF THIS CODE AND ITS DATA PACKAGE WITHOUT SPECIFIC, WRITTEN PRIOR AUTHORIZATION.  PERMISSION TO MODIFY 
% OR OTHERWISE CREATE DERIVATIVE WORKS OF THIS CODE AND ITS DATA PACKAGE IS NOT GRANTED.
% 
% THIS USE OF  THIS CODE AND ITS DATA PACKAGE REQUIRES MATLAB(TM) AND MICROSOFT EXCEL. THE  THIS CODE AND ITS DATA PACKAGE USER IS SOLELY
% RESOPNSIBLE FOR OBTAINTING AN APPROPRIATE LICENSE FOR MATLAB(TM) AND MICROSOFT EXCEL.  NO LICENSE TO MATLAB(TM) OR MICROSOFT EXCEL IS 
% GRANTED, INFERRED OR IMPLIED BY THE DISTRIBUTION OF THIS CODE AND ITS DATA PACKAGE.
% 
% THIS CODE AND ITS DATA PACKAGE IS PROVIDED AS IS, WITHOUT REPRESENTATION AS TO ITS FITNESS FOR ANY PURPOSE, AND WITHOUT WARRANTY OF ANY 
% KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR 
% PURPOSE. THE REGENTS OF THE UNIVERSITY OF MICHIGAN SHALL NOT BE LIABLE FOR ANY DAMAGES, INCLUDING SPECIAL, INDIRECT,INCIDENTAL, OR 
% CONSEQUENTIAL DAMAGES, WITH RESPECT TO ANY CLAIM ARISING OUT OF OR IN CONNECTION WITH THE USE OF THE CODE AND ITS DATA PACKAGE, EVEN IF 
% IT HAS BEEN OR IS HEREAFTER ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
% ******************************************************************************************

% Data available at https://deepblue.lib.umich.edu/handle/2027.42/116203?show=full

% Heat-release code, Gabriel Turesson 2018

% It computes 3 different heat-release rate for a pressure curve logged every 0.1 CAD.

% dQ_a -> apparent heat-release rate (without heat transfer)
% dQ -> heat-release rate with heat transfer
% dQ_gamma -> heat-release rate with heat-transfer and temperature dependent gamma

% Q_ht is the heat-transfer rate.

% The data is taken from intake-valve closing (IVC) to exhaust-valve
% opening (EVO).

% Heat-transfer model from heywood
% gamma computed from NASA polynomials

% Important tuning parameters are rc, tdc_offset and the filter cut-off
% frequency.

clear all
close all

load p_data

for k = 1:8; % 8 different operating points

p = P(k,:);  % p in Pa
lambda = Lambda(k,:);
speed = Speed(k); % engine speed [rpm]

% Model Inputs
Tin = 320; % Intake-manifold temperature at IVC [K]
Tw = 400; % cylinder-wall temperature [K]
tdc_offset = 0; % TDC offset, (affects the volume curve) [CAD]

% Cylinder Geometry

l = 268e-3;  % connecting rod [m^3]
B = 132e-3;  % bore [m^3]
a = 0.5*156e-3; % crank radius
L = 2*a;
Vd = pi*(B/2)^2*L; % displacement volume [m^3]
rc = 17.3; % compression ratio
Vc = 1/(rc-1)*Vd;  % clearance volume 
R = l/a;    
IVC = -160;
EVO = 140;

res = 0.1; % pressure-signal resolution [CAD]
CAD = (IVC:res:EVO) + tdc_offset; % crank-angle degree from intake-valve closing to exhaust-valve opening with a 0.2 resolution

V = Vc + Vc/2*(rc-1)*(R + 1 - cos(pi*CAD/180) - (R^2 - sin(pi*CAD/180).^2).^(1/2)); % cylinder volume
 
gamma = 1.35; % assumed constant gamma
nR = p(1)*V(1)/Tin; % compute the nR constant in ideal gas law nRT = PV
T = p.*V/nR; % in-cylinder temperature by the assumption of the ideal gas law and constant mass. (approximation due to fuel injection).

p_mot = p(1)*(V(1)./V).^gamma; % assumed motored pressure curve, should be compared to motored engine data.

% derivative computation
dV = gradient(V,res);
dp = gradient(p,res);

% filtering of pressure derivative
[B_f,A_f] = butter(4,0.1); % filter coefficients
dp = filtfilt(B_f,A_f,dp); % zero-phase filtering

% Heat-Transfer Model, see Heywood p. 680.  dQ_ht = hc*A*(T-Tw),
% hc is a heat-transfer coefficient, Tw wall temp. and A cylinder area.

Ach = B^2/4*pi; % cylinder head area
Ap = Ach; % piston area
A = Ach + Ap + pi*B*L/2*(R + 1 - cos(pi*CAD/180) - (R^2 - sin(pi*CAD/180).^2).^(1/2)); % total area
Sp = 2*L*speed/60; % mean piston speed [m/s]

C1 = 2.28; %tuning parameter
C2 = 0.0032; %tuning parameter 

w =  C1*Sp + C2*(CAD > 0).*((Vd*Tin)/(V(1)*p(1))).*max(p-p_mot,0); % average cylinder velocity
hc = 3.26*B.^(-0.2).*(p/1000).^(0.8).*T.^(-0.55).*w.^0.8; % convective heat-transfer coefficient
conv = 60/(360*1200); % conversion from time to CAD
dQ_f = hc.*(T-Tw)/(10^6); % heat-transfer flux [MW/m^2] (data in Heywood are given in this unit);
dQ_ht = hc.*A.*(T-Tw)*conv; % heat-transfer rate
Q_ht = cumsum(dQ_ht)*res;

dQ_a = gamma./(gamma-1).*p.*dV + 1./(gamma-1).*V.*dp; % apparent heat-release rate (without heat-transfer)
dQ = gamma./(gamma-1).*p.*dV + 1./(gamma-1).*V.*dp + dQ_ht; % net heat-release rate (with heat-transfer)

dQ_a(1:200) = 0; % removed noise close to IVC
dQ(1:200) = 0; 

Q_a = cumsum(dQ_a)*res; 
Q = cumsum(dQ)*res; % accumulated heat-release

% compute CA50, CA10, CA90
[~,ind_CA50] = min(abs(Q - 0.5*max(Q)));
CA50 = CAD(ind_CA50);
[~,ind_CA90] = min(abs(Q - 0.9*max(Q)));
CA90 = CAD(ind_CA90);
[~,ind_CA10] = min(abs(Q - 0.1*max(Q)));
CA10 = CAD(ind_CA10);
CA90_10 = CA90-CA10;

% compute gamma from gas composition and temperature, gamma = cp(T)/cv(T).

R = 8.3144621;

% cp polynomials in T
cp_CO2 = (T <= 1000).*(0.24008*10 + 0.87351*10^(-2)*T - 0.66071*10^(-5)*T.^2 + 0.2002*10^(-8)*T.^3 + 0.63274*10^(-15)*T.^4)*R + (T > 1000).*(0.446*10 + 0.309*10^(-2)*T - 0.123*10^(-5)*T.^2 + 0.227*10^(-9)*T.^3  - 0.155*10^(-13)*T.^4)*R; 
cp_H2O = (T <= 1000).*(0.4071*10  - 0.11084*10^(-2)*T + 0.4152*10^(-5)*T.^2 - 0.29637*10^(-8)*T.^3 + 0.80702*10^(-12)*T.^4)*R + (T > 1000).*(0.271*10  + 0.294*10^(-2)*T - 0.802*10^(-6)*T.^2 + 0.102*10^(-9)*T.^3 - 0.48*10^(-14)*T.^4)*R;
cp_O2 =  (T <= 1000).*(0.36256*10  - 0.18782*10^(-2)*T + 0.7055*10^(-5)*T.^2 - 0.67635*10^(-8)*T.^3 + 0.21556*10^(-11)*T.^4)*R + (T > 1000).*(0.36256*10  + 0.73*10^(-3)*T - 0.19*10^(-6)*T.^2 + 0.36*10^(-10)*T.^3 - 0.289*10^(-14)*T.^4)*R;
cp_N2 =  (T <= 1000).*(0.36748*10  - 0.12082*10^(-2)*T + 0.2324*10^(-5)*T.^2 - 0.63218*10^(-9)*T.^3 - 0.22577*10^(-12)*T.^4)*R + (T > 1000).*(0.289*10  + 0.15082*10^(-2)*T - 0.572*10^(-6)*T.^2 + 0.99*10^(-10)*T.^3 - 0.65*10^(-14)*T.^4)*R;
y = 2.28; % H to C (2.2857 for n-heptane)
eps = 4/(4+y);
psi = 3.773;
cp_ub = 1/4.774*(3.773*cp_N2 + cp_O2); % cp unburnt gas
cp_b = (eps/lambda*cp_CO2 + 2*(1-eps)/lambda*cp_H2O + (1-1/lambda)*cp_O2 + psi*cp_N2)/((1-eps)/lambda + 1 + psi); % cp burnt gas

gamma_ub = cp_ub./(cp_ub-R); % cp unburnt gas
gamma_b = cp_b./(cp_b-R); % cp burnt gas
dist = normcdf(CAD,CA50,CA90_10/4); % interpolation between unburnt and burtn gas, based on computed combustion timing and duration
gamma_new = gamma_ub.*(1-dist) + gamma_b.*dist;

% recompute heat-release rate with updated gamma

dQ_gamma = gamma_new./(gamma_new-1).*p.*dV + 1./(gamma_new-1).*V.*dp + dQ_ht; % net heat-release rate (with heat-transfer)
dQ_gamma(1:200) = 0; % removed noise close to IVC

Q_gamma = cumsum(dQ_gamma)*res;

% compute CA50
[~,ind_CA50] = min(abs(Q_gamma - 0.5*max(Q_gamma)));
CA50 = CAD(ind_CA50);

% plot results

cmap = [0.21569,0.49412,0.72157;0.89412,0.10196,0.10980];

figure(1);clf
set(gcf,'color','white')
figPos = get(0,'defaultfigureposition');
width = 600;
height = 600;
set(gcf,'Position', [200, 200, width, height]);


subplot(311);hold on
plot(CAD,p,'linewidth',2,'color',cmap(1,:))
plot(CAD,p_mot,'k--','linewidth',2)
ylabel('p [pa]')
xlabel('\theta [CAD]')
title('Pressure')
% xlim([-50 50])
legend('fired','motored','location','northwest')
box on;grid on

subplot(312);hold on
plot(CAD,dQ,'linewidth',1,'color',cmap(1,:))
plot(CAD,dQ_a,'-.','linewidth',2,'color',cmap(1,:))
plot(CAD,dQ_ht,'--','linewidth',2,'color',cmap(1,:))
plot(CAD,dQ_gamma,'-','linewidth',2,'color',cmap(2,:))
plot(CA50,dQ(ind_CA50),'x','markersize',12,'linewidth',2,'color',cmap(2,:))
ylim([0 800])
title('Heat-Release Rate')
ylabel('dQ/d\theta [J/CAD]')
xlabel('\theta [CAD]')
% xlim([-50 50])
box on;grid on
legend('dQ/d\theta','dQ_a/d\theta','dQ_{ht}/d\theta','dQ_{\gamma}/d\theta','location','northwest')

subplot(313);hold on
plot(CAD,Q,'linewidth',2,'color',cmap(1,:))
plot(CAD,Q_a,'-.','linewidth',2,'color',cmap(1,:))
plot(CAD,Q_ht,'--','linewidth',2,'color',cmap(1,:))
plot(CAD,Q_gamma,'--','linewidth',2,'color',cmap(2,:))
legend('Q','Q_a','Q_{ht}','Q_{\gamma}','location','northwest')
plot(CA50,Q(ind_CA50),'x','markersize',12,'linewidth',2,'color',cmap(2,:))
title('Accumulated Heat-Release')
ylabel('Q [J]')
xlabel('\theta [CAD]')
ylim([0 8000])
% xlim([-50 50])
box on;grid on

figure(2)
set(gcf,'color','white')
figPos = get(0,'defaultfigureposition');
width = 300;
height = 300;
set(gcf,'Position', [850, 500, width, height]);

plot(CAD,dQ_f,'linewidth',2,'color',cmap(2,:))
ylabel('dQ_f [MW/m^2]')
title('heat-transfer flux')
xlabel('\theta [CAD]')
ylim([0 5])
% xlim([-50 50])
box on;grid on

figure(3)
set(gcf,'color','white')
figPos = get(0,'defaultfigureposition');
width = 300;
height = 300;
set(gcf,'Position', [1200, 500, width, height]);

plot(CAD,gamma_new,'linewidth',2,'color',cmap(2,:))
ylabel('\gamma')
title('Temperature dependent \gamma')
xlabel('\theta [CAD]')
ylim([1 2])
% xlim([-50 50])
box on;grid on

[~,ind_CA50] = min(abs(Q_a - 0.5*max(Q_a)));
CA50_a = CAD(ind_CA50)
[~,ind_CA50] = min(abs(Q - 0.5*max(Q)));
CA50 = CAD(ind_CA50)
[~,ind_CA50] = min(abs(Q_gamma - 0.5*max(Q_gamma)));
CA50_gamma = CAD(ind_CA50)

disp('click for next pressure curve')
w = waitforbuttonpress;
end
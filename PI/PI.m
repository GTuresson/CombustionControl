% PI controller simulation
% PI controllers used to control IMEP and CA50

% Ingesson, Gabriel, et al. "Proportional–Integral Controller Design for Combustion-Timing Feedback, From n-Heptane to Iso-Octane in Compression–Ignition Engines."
% Journal of Dynamic Systems, Measurement, and Control 140.5 (2018): 054502.

clear; clc
close all

cmap = [0.21569,0.49412,0.72157;0.89412,0.10196,0.10980];

N_cycles = 80;

%%%%%%%%%%%%%%%%%%%%%% Simulation Parameters %%%%%%%%%%%%%%%%%%%%%%%%%%

ode_options = odeset('RelTol',10^-4,'AbsTol',10^-8);    

% simulated cycle-to-cycle variation

params.CA50_noise_std = 0.25; % [CAD]
params.mf_noise_std = 2; % [mg]

% set simulation parameters

params.sim_res = 0.02;
params.res = 0.2;
CAD_sim = -151:params.sim_res:146; % for simulation
CAD = -151:params.res:146; % for sampling

params.l = 255*10^-3; % connecting-rod length
params.L = 160*10^-3; % stroke
params.B = 130*10^-3; % bore
params.rc = 17; % compression ratio
params.C1 = 2.28; % heat-transfer coeff
params.C2 = 0.0032;  % heat-transfer coeff
params.IVC = -151;
params.EVO = 146;
params.V_ivc = 0.002166582207529; % V at IVC
params.EGR = 0; % EGR
params.speed = 1200; % engine speed 
params.CAD = CAD_sim;
params.Tw = 400; % wall temp
params.Q_LHV = 45*10^6; % fuel heating value 

CA50_ref = [10*ones(1,N_cycles/4) 5*ones(1,N_cycles/4) 5*ones(1,N_cycles/4) 5*ones(1,N_cycles/4)];      % CA50 reference
IMEP_ref = [10*ones(1,N_cycles/4) 10*ones(1,N_cycles/4) 5*ones(1,N_cycles/4) 10*ones(1,N_cycles/4)];    % IMEP reference

% Controller parameters
kp_CA50 = 0.6;
kI_CA50 = 0.4;

kp_IMEP = 2;
kI_IMEP = 5;

% I-part initialization
I_CA50 = 0;
I_IMEP = 50;

% control signal u(1) = start of injection [CAD], u(2) = fuel mass [mg]
u = [0; 50];

figure(1);clf
set(gcf,'color','white')
figPos = get(0,'defaultfigureposition');
width = 600;
height = 600;
set(gcf,'Position', [200, 200, width, height]);

for i = 1:N_cycles
    
%%%%%%%%%%% Run Simulation %%%%%%%%%%%%%%%%%%%%%

params.soi = u(1); % set inputs and conditions
params.mf = u(2);
set_initial_conditions; % intake conditions mapped from mf
[params.dQ,params.Q] = compute_dQ(params); % computes ignition delay and heat-release rate
P0 = [params.p0,params.p0]';
[~,P] = ode23(@(t,y)dp(t,y,params),CAD_sim,P0,ode_options); %simulates fired and motored pressure trace

U(:,i) = u;

p = P(:,1);
p = p(1:10:end); % downsampling;

%%%%%%%%%%%%%%%%%%%%% plot pressure %%%%%%%%%%%%%%%%%%%%%%%%

figure(1);clf;hold on
plot(CAD,p*10^-5,'color',cmap(1,:),'linewidth',2)

%%%%%%%%%%%%%%%%%%%%% Heat-Release Analysis %%%%%%%%%%%%%%%%
tdc_offset = 0;
[dQ,CA50,IMEP] = HRA(p,params,tdc_offset);

CA50S(i) = CA50;
IMEPS(i) = IMEP;

plot(CAD,dQ/10,'color',cmap(1,:),'linewidth',2)
plot([CA50 CA50],[0 200],'color',cmap(1,:),'linewidth',2);
plot([CA50_ref(i) CA50_ref(i)],[0 200],'k--','linewidth',2);

ylim([0 150])
xlim([-20 50])

%%%%%%%%%%%%%%%%%%%%% Controllers %%%%%%%%%%%%%%%%%%%%%%%%%

% PI controller
u(1) = kp_CA50*(CA50_ref(i) - CA50) + I_CA50; 
I_CA50 = I_CA50 + kI_CA50*(CA50_ref(i) - CA50);


% IMEP controller
u(2) = kp_IMEP*(IMEP_ref(i) - IMEP) + I_IMEP; 
I_IMEP = I_IMEP + kI_IMEP*(IMEP_ref(i) - IMEP);

xlabel('\theta [CAD]')
ylabel('p [bar]')

end


% plot results

figure(2);clf
set(gcf,'color','white')
figPos = get(0,'defaultfigureposition');
width = 600;
height = 700;
set(gcf,'Position', [800, 200, width, height]);
cmap = [0.21569,0.49412,0.72157;0.89412,0.10196,0.10980];

figure(2);clf
subplot(411);hold on
plot(CA50_ref,'k--','linewidth',2)
plot(CA50S,'color',cmap(1,:),'linewidth',2)
legend('\theta_{50}^{ref}','\theta_{50}','orientation','horizontal','location','southwest')
xlabel('cycle [-]')
ylabel('\theta_{50} [CAD]')
ylim([-15 20])
xlim([0 N_cycles])

subplot(412);hold on
plot(U(1,:),'color',cmap(1,:),'linewidth',2)
xlabel('cycle [-]')
ylabel('\theta_{SOI} [CAD]]')
ylim([-10 20])
xlim([0 N_cycles])

subplot(413);hold on
plot(IMEP_ref,'k--','linewidth',2)
plot(IMEPS,'color',cmap(1,:),'linewidth',2)
legend('p_{IMEP}^{ref}','p_{IMEP}','orientation','horizontal','location','southwest')

xlabel('cycle [-]')
ylabel('p_{IMEP} [bar]')
ylim([0 20])
xlim([0 N_cycles])

subplot(414);hold on
plot(U(2,:),'color',cmap(1,:),'linewidth',2)
xlabel('cycle [-]')
ylabel('m_f [mg]')
ylim([0 150])
xlim([0 N_cycles])

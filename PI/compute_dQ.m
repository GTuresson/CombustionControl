function [dQ,Q] = compute_Q(params)

tau = ignition_delay(params) + randn*params.CA50_noise_std;

Q_tot = (params.mf + randn*params.mf_noise_std)*params.Q_LHV*10^-6;
soi = round(params.soi/0.02)*0.02;

% combustion duration is mapped from fuel mass

CDUR_min = 15;
CDUR_max = 45;
mf_max = 200;

t_mf = max(min(params.mf,mf_max),0)/mf_max;

CDUR = t_mf*CDUR_max + (1-t_mf)*CDUR_min;
CDUR = round(CDUR/0.02)*0.02;

% Watson, N., A. D. Pilley, and Mohamed Marzouk. A combustion correlation for diesel engine simulation. No. 800029. SAE Technical Paper, 1980.

SOC = soi + tau;

t = SOC:0.02:SOC + CDUR;
t = (t - SOC)/CDUR;

a = 0.8;
b = 0.3;
c = 0.4;
conv = params.speed/60*360*10^-3;
beta = 1 - a*params.lambda^(-b)*(tau/conv)^(-c);
beta = max(min(beta,1),0);
K1 = 2 + 1.25*10^-8*(tau/conv*params.speed)^2.4;
K2 = 5000;
K3 = 14.2*params.lambda^(0.644);
K4 = 0.79*K3^0.25;

f1 = 1 - (1-t.^K1).^K2;
f2 = 1 - exp(-K3*t.^K4);

Q = (beta*f1 + (1-beta)*f2)*Q_tot;
N1 = size(params.IVC:params.sim_res:(SOC-0.02),2);
M2 = size((SOC + CDUR):params.sim_res:params.EVO,2);

Q = [zeros(1,N1) Q Q_tot*ones(1,M2)];
dQ = gradient(Q,0.02);

% figure(10);clf;hold on
% plot(CAD,Q)
% plot(CAD,dQ)

end